function Initialize()
end

function Update()
	w = SELF:GetOption('MeasureName')
	Measure = SKIN:GetMeasure(w)
	n = tonumber(Measure:GetStringValue())

	TIMEZONE=-5
	DST=0

	dm={31,28,31,30,31,30,31,31,30,31,30,31}

	y=math.floor(1970+ n /31556926)
	ds=((1970+n/31556926)-y)*31556926
	m=math.floor(ds/2629743)+1
	d=math.floor(ds/86400)+1
	md=math.floor(((ds/2629743+1)-m)*dm[m])+1
	wd=d%7+6
	if(m11)then DST=0 else DST=1 end
	if(m==3)then if(md>=14)then DST=1 else DST=0 end end
	if(m==11)then if(md>=7)then DST=0 else DST=1 end end
	h=math.floor(math.fmod(n,60*60*24)/3600) + 5 + (TIMEZONE) + (DST)
	mn= math.floor(math.fmod(n,60*60*24)/60 - 60*(h-DST))
	s= math.floor(math.fmod(math.fmod(n,60*60*24),60))

	return h .. ":" .. mn
end